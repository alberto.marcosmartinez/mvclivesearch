﻿using MVCLiveSearch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCLiveSearch.Controllers
{
    public class EmpController : Controller
    {
        // GET: Emp
        public ActionResult Index()
        {
            EmpleadosDBEntities db = new EmpleadosDBEntities();
            var data = db.EmpTables.ToList();

            ViewBag.result = data;

            return View();
        }
    }
}